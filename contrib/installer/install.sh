#!/bin/sh

LIBDIR=/usr/lib/bbcloner
BINDIR=/usr/bin
MANDIR=/usr/share/man/man1/


if [ "$(id -u)" != "0" ]; then
    echo "You are not root. Exiting..."
    exit 1
fi

mkdir -p "$LIBDIR"
mkdir -p "$BINDIR"

echo "Copying libraries to $LIBDIR..."
cp bbcloner "$LIBDIR"
cp -ar requests "$LIBDIR"

echo "Installing binary in $BINDIR/bbcloner"
ln -s $LIBDIR/bbcloner $BINDIR/bbcloner

echo "Installing manpage"
cp bbcloner.1 $MANDIR

echo "Done."
