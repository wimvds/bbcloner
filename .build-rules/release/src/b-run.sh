#!/bin/sh

VERSION=$1
NAME=bbcloner
TARGET_DIR=releases/src/$NAME-$VERSION

if [ -z $VERSION ]; then
    echo "Version required for this build rule" >&2
    exit
fi

if [ -d $TARGET_DIR ]; then
    rm -rf $TARGET_DIR
fi

# Create the release directory structure.
mkdir -p $TARGET_DIR

# Copy the source to the release directory structure.
cp LICENSE $TARGET_DIR/
cp README.md $TARGET_DIR/
cp src/bbcloner $TARGET_DIR/
cp doc/bbcloner.1 $TARGET_DIR/
cp -ar releases/cache/requests-1.2.0/requests $TARGET_DIR/requests
cp contrib/installer/*.sh $TARGET_DIR/

# Remove some files / directories we don't want in the release.
find releases/ -name ".svn" -type d -print0 | xargs -0 /bin/rm -rf
find releases/ -name "*.pyc" -type f -print0 | xargs -0 /bin/rm -rf

# Bump version numbers
find releases/ -type f -print0 | xargs -0 sed -i "s/%%VERSION%%/$VERSION/g" 

# Create package releases.
cd $TARGET_DIR/..
tar -czf $NAME-$VERSION.tar.gz $NAME-$VERSION/
zip -q -r $NAME-$VERSION.zip $NAME-$VERSION/
